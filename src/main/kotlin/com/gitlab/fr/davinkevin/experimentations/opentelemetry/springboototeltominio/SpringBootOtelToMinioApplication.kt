package com.gitlab.fr.davinkevin.experimentations.opentelemetry.springboototeltominio

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringBootOtelToMinioApplication

fun main(args: Array<String>) {
	runApplication<SpringBootOtelToMinioApplication>(*args)
}
